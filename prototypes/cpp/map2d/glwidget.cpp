#include "glwidget.h"

#include <GL/glu.h>

GLWidget::GLWidget(QWidget *parent) :
  QGLWidget(parent),
  m_map( 0 ),
  m_camera( QVector3D( 0.0, 0.0, -1.0 ) ),
  m_mousePressStart( QPoint() ),
  m_movingCamera( false ),
  m_mousePressCamStartPos(),
  m_settingType( false ),
  m_currentType( 0 )
{
  setMouseTracking( true );
}

void GLWidget::initializeGL()
{
  glClearColor( 0.0, 0.0, 0.0, 1.0 );
  glEnable( GL_DEPTH_BUFFER_BIT );
  glEnable( GL_TEXTURE_2D );
  m_map = new Map( this );
}

void GLWidget::resizeGL(int w, int h)
{
  /*
  w/h = 20/x
    */
  glViewport( 0, 0, static_cast< GLsizei >( w ), static_cast< GLsizei >( h ) );
  glMatrixMode( GL_PROJECTION_MATRIX );
  glLoadIdentity();
  if ( w > 0 ) {
    glOrtho( 0, 20, 0, 20 *h / w, 0.0, 100.0 );
  }
  glTranslatef( m_camera.x(), m_camera.y(), m_camera.z() );
  glMatrixMode( GL_MODELVIEW );
}

void GLWidget::paintGL()
{
  resizeGL( width(), height() );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  m_map->render();
  QTimer::singleShot( 100, this, SLOT(repaint()) );
}

void GLWidget::wheelEvent(QWheelEvent *ev)
{
  m_camera.setZ( m_camera.z() + ev->delta() * 0.1 );
}

void GLWidget::mousePressEvent(QMouseEvent *ev)
{
  if ( ev->button() == Qt::LeftButton ) {
    m_movingCamera = true;
    m_mousePressCamStartPos = m_camera;
    m_mousePressStart = ev->globalPos();
  } else if ( ev->button() == Qt::RightButton ) {
    m_settingType = true;
  }
}

void GLWidget::mouseReleaseEvent(QMouseEvent *ev)
{
  if ( ev->button() == Qt::LeftButton ) {
    m_movingCamera = false;
    applyPos( ev->globalPos() );
  } else if ( ev->button() == Qt::RightButton ) {
    m_settingType = false;
  }
}

void GLWidget::mouseMoveEvent(QMouseEvent *ev)
{
  if ( m_movingCamera ) {
    applyPos( ev->globalPos() );
  } else if ( m_settingType ) {
    QVector3D pos = getWorldCoordinates( ev->pos() );
    setWindowTitle( QString( "Mouse: x=%1 y=%2 z=%3" ).arg( pos.x() ).arg( pos.y() ).arg( pos.z() ) );
    if ( pos.x() >= 0 && pos.x() < m_map->width() && pos.y() >= 0 && pos.y() < m_map->height() ) {
      if ( m_currentType ) {
        m_map->setField( static_cast< int >( floor( pos.x() ) ),
                         static_cast< int >( floor( pos.y() ) ),
                         m_currentType );
      }
    }
  }
}

void GLWidget::keyReleaseEvent(QKeyEvent *ev)
{
  int idxOff = 0;

  switch ( ev->key() ) {
  case Qt::Key_Escape:
  {
    m_camera = QVector3D( 0.0, 0.0, -1.0 );
    break;
  }

  case Qt::Key_Up: idxOff = 1;
  case Qt::Key_Down:
  {
    if ( idxOff ) {
      idxOff = -1;
    }
    int currentIdx = m_map->types().indexOf( m_currentType );
    currentIdx += idxOff;
    if ( !m_map->types().isEmpty() ) {
      m_currentType = m_map->types().at( ( currentIdx + m_map->types().size() ) % m_map->types().size() );
    }
  }
  default: break;
  }
}

void GLWidget::applyPos(QPoint currentPos)
{
  m_camera.setX( m_mousePressCamStartPos.x() + ( currentPos.x() - m_mousePressStart.x() ) * 0.01 );
  m_camera.setY( m_mousePressCamStartPos.y() + ( currentPos.y() - m_mousePressStart.y() ) * - 0.01 );
  setWindowTitle( QString( "Cam: x=%1 y=%2 z=%3" ).arg( m_camera.x() ).arg( m_camera.y() ).arg( m_camera.z() ) );
}

QVector3D GLWidget::getWorldCoordinates(QPoint pos)
{
  // See: http://wiki.delphigl.com/index.php/gluUnProject
  GLdouble result[3];
  GLint viewport[4];
  GLdouble modelview[ 16 ];
  GLdouble projection[ 16 ];
  GLfloat z;
  int y;

  glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
  glGetDoublev( GL_PROJECTION_MATRIX, projection );
  glGetIntegerv( GL_VIEWPORT, viewport );

  y = viewport[ 3 ] - pos.y();

  glReadPixels( pos.x(), y, 1, 1, GL_DEPTH_COMPONENT, GL_FLAT, &z );
  gluUnProject( pos.x(), y, z, modelview, projection, viewport, &result[0], &result[1], &result[2] );

  return QVector3D( result[ 0 ], result[ 1 ], result[ 2 ] );
}
