#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "map.h"

#include <QGLWidget>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>

class GLWidget : public QGLWidget
{
  Q_OBJECT
public:
  explicit GLWidget(QWidget *parent = 0);
  
signals:
  
public slots:

protected:

  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();

  void wheelEvent(QWheelEvent *ev);
  void mousePressEvent(QMouseEvent *ev);
  void mouseReleaseEvent(QMouseEvent *ev);
  void mouseMoveEvent(QMouseEvent *ev);
  void keyReleaseEvent(QKeyEvent *ev);

private:

  Map* m_map;
  QVector3D m_camera;
  QPoint m_mousePressStart;
  bool m_movingCamera;
  QVector3D m_mousePressCamStartPos;
  bool m_settingType;
  MapField* m_currentType;

  void applyPos( QPoint currentPos );

  QVector3D getWorldCoordinates( QPoint pos );

};

#endif // GLWIDGET_H
