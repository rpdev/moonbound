#include <QApplication>

#include "glwidget.h"

int main( int argc, char** argv ) {
  QApplication app( argc, argv );

  GLWidget* widget = new GLWidget();
  widget->resize( 800, 600 );
  widget->show();

  return app.exec();
}
