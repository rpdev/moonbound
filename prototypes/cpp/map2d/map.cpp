#include "map.h"

Map::Map(QGLWidget *parent) :
  QObject(parent),
  m_width( 0 ),
  m_height( 0 ),
  m_fields(),
  m_fieldTypes()
{
  // Load types
  QDir dir( ":/images" );
  foreach ( const QString& entry, dir.entryList( QStringList() << "*.bmp", QDir::Files ) ) {
    QPixmap bmp( dir.absoluteFilePath( entry ) );
    MapField* field = new MapField( this );
    field->setTexture( parent->bindTexture( bmp ) );
    m_fieldTypes << field;
  }

  // initialize map
  resize( 20, 20 );
}

int Map::width() const
{
  return m_width;
}

int Map::height() const
{
  return m_height;
}

MapField *Map::field(int x, int y) const
{
  if ( x >= 0 && x < m_width && y >= 0 && y < m_height ) {
    return m_fields.at( x * m_width + y );
  }
  return 0;
}

const QVector<MapField *> &Map::types() const
{
  return m_fieldTypes;
}

void Map::render()
{
  QVector< QVector3D > positions;
  foreach ( MapField* fieldType, m_fieldTypes ) {
    positions.clear();
    positions.reserve( m_width * m_height );
    for ( int x = 0; x < m_width; ++x ) {
      for ( int y = 0; y < m_height; ++y ) {
        if ( field( x, y ) == fieldType ) {
          positions << QVector3D( x, y, 0.0 );
        }
      }
    }
    fieldType->render( positions );
  }
}

void Map::setField(int x, int y, MapField *field)
{
  if ( x >= 0 && x < m_width && y >= 0 && y < m_height ) {
    m_fields[ x * m_width + y ] = field;
  }
}

void Map::resize(int newWidth, int newHeight)
{
  if ( newWidth >= 0 && newHeight >= 0 ) {
    m_fields.resize( newWidth * newHeight );
  }
  m_fields.fill( m_fieldTypes.isEmpty() ? 0 : m_fieldTypes.first() );
  m_width = newWidth;
  m_height = newHeight;
}

void Map::setWidth(int newWidth)
{
  resize( newWidth, height() );
}

void Map::setHeight(int newHeight)
{
  resize( width(), newHeight );
}


