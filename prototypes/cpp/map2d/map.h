#ifndef MAP_H
#define MAP_H

#include "mapfield.h"

#include <QObject>
#include <QtOpenGL>
#include <QVector>

class Map : public QObject
{
  Q_OBJECT
public:
  explicit Map( QGLWidget *parent = 0);


  int width() const;
  int height() const;
  MapField* field( int x, int y ) const;
  const QVector< MapField* >& types() const;

  void render();

  
signals:
  
public slots:

  void setField( int x, int y, MapField* field );
  void resize( int newWidth, int newHeight );
  void setWidth( int newWidth );
  void setHeight( int newHeight );

private:

  int m_width;
  int m_height;

  QVector< MapField* > m_fields;
  QVector< MapField* > m_fieldTypes;

};

#endif // MAP_H
