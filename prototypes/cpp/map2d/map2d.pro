TEMPLATE = app
TARGET = map2d_prototype

CONFIG += qt
QT += core gui opengl

SOURCES += \
    main.cpp \
    mapfield.cpp \
    map.cpp \
    glwidget.cpp

RESOURCES += \
    resources.qrc

HEADERS += \
    mapfield.h \
    map.h \
    glwidget.h

LIBS += -lGLU
