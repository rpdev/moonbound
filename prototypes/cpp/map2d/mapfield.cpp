#include "mapfield.h"

MapField::MapField(QObject *parent) :
  QObject(parent),
  m_texture( 0 )
{
}

GLuint MapField::texture() const
{
  return m_texture;
}

void MapField::render(const QVector<QVector3D> &positions)
{
  static const Vertex vertices[4] = { { 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0 },
                                      { 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0 },
                                      { 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0 },
                                      { 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0 } };

  glPushAttrib( GL_TEXTURE_BIT | GL_CURRENT_BIT );
  glBindTexture( GL_TEXTURE_2D, m_texture );
  //glInterleavedArrays( GL_T2F_N3F_V3F, sizeof(Vertex), reinterpret_cast< const GLvoid* >( vertices ) );
  foreach ( const QVector3D& pos, positions ) {

    glPushMatrix();
    glTranslated( pos.x(), pos.y(), pos.z() );
    //glDrawArrays( GL_QUADS, 0, 4 );
    glBegin( GL_QUADS );
    for ( int i = 0; i < 4; ++i ) {
      glTexCoord2d( vertices[ i ].s, vertices[ i ].t );
      glNormal3d( vertices[ i ].nx, vertices[ i ].ny, vertices[ i ].nz );
      glVertex3d( vertices[ i ].x, vertices[ i ].y, vertices[ i ].z );
    }
    glEnd();
    glPopMatrix();

  }
  glPopAttrib();
}

void MapField::setTexture(GLuint texture)
{
  m_texture = texture;
}
