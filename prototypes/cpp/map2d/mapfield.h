#ifndef MAPFIELD_H
#define MAPFIELD_H

#include <QObject>
#include <QtOpenGL>
#include <QVector>
#include <QVector3D>

class MapField : public QObject
{
  Q_OBJECT
  Q_PROPERTY(GLuint texture READ texture WRITE setTexture)
public:

  explicit MapField(QObject *parent = 0);

  GLuint texture() const;

  void render( const QVector< QVector3D >& positions );
  
signals:
  
public slots:

  void setTexture( GLuint texture );

private:

  GLuint m_texture;

  struct Vertex {
    GLdouble s;
    GLdouble t;
    GLdouble nx;
    GLdouble ny;
    GLdouble nz;
    GLdouble x;
    GLdouble y;
    GLdouble z;
  };
  
};

#endif // MAPFIELD_H
