// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: container
    state: "UP"

    property string text: "Button"
    property bool checked: false
    property bool checkable: false
    property Item group: null

    signal clicked

    onClicked: {
        if ( checkable ) {
            checked = !checked;
        }
    }

    onCheckedChanged: {
        container.state = checked ? "DOWN" : "UP";
        if ( group && checked ) {
            group.activeButton = container;
        }
    }

    Component.onCompleted: {
        if ( group ) {
            group.activeButtonChanged.connect( setState );
        }
    }

    states: [
        State {
            name: "UP"
            PropertyChanges { target: gradientStart; color: activePalette.light }
        },
        State {
            name: "DOWN"
            PropertyChanges { target: gradientStart; color: activePalette.dark }
        }
    ]

    transitions: [
        Transition {
            from: "UP"
            to: "DOWN"
            ColorAnimation { target: gradientStart; duration: 50 }
        },
        Transition {
            from: "DOWN"
            to: "UP"
            ColorAnimation { target: gradientStart; duration: 50 }
        }

    ]

    width: buttonLabel.width + 20; height: buttonLabel.height + 5
    border { width: 1; color: Qt.darker( activePalette.button ) }
    smooth: true
    radius: 8

    gradient: Gradient {
        GradientStop {
            id: gradientStart
            position: 0.0
        }
        GradientStop {
            id: gradientStop
            position: 1.0
            color: activePalette.button
        }
    }

    function setState() {
        if ( group && checkable ) {
            container.checked = ( group.activeButton == container );
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: container.clicked()
        onPressed: container.state = "DOWN"
        onReleased: container.state = "UP"
    }

    Text {
        id: buttonLabel
        anchors.centerIn: container
        color: activePalette.buttonText
        text: container.text
    }

}
