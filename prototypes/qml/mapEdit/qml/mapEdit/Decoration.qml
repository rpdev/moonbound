// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: block
    width: 64
    height: 64
    z: 1

    property string source: img.source

    onSourceChanged: img.source = source;

    Image {
        id: img
        anchors.fill: parent
        source: "../../images/tree_light_1.png"
    }
}
