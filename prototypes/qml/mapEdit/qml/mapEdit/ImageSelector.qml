// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Repeater {
    id: selector;

    property Item currentItem;

    model: imageList;

    Item {
        id: item;
        width: 64;
        height: 64;
        state: "DEACTIVATED";

        property string imageSource: modelData;

        function setActiveState() {
            item.state = ( selector.currentItem == item ) ? "ACTIVATED" : "DEACTIVATED";
        }

        Component.onCompleted: {
            selector.currentItemChanged.connect( item.setActiveState );
        }

        states: [
            State {
                name: "DEACTIVATED"
                PropertyChanges { target: rect; border.color: "transparent" }
            },
            State {
                name: "ACTIVATED"
                PropertyChanges { target: rect; border.color: "red" }
            }
        ]

        transitions: [
            Transition {
                from: "DEACTIVATED"
                to: "ACTIVATED"
                ColorAnimation { target: rect; duration: 500 }
            },
            Transition {
                from: "ACTIVATED"
                to: "DEACTIVATED"
                ColorAnimation { target: rect; duration: 500 }
            }
        ]

        Rectangle {
            id: rect;
            anchors.fill: parent;
            border { width: 1; color: "black" }
            color: "transparent"

            Image {
                x: 1;
                y: 1;
                width: parent.width - parent.border.width;
                height: parent.height - parent.border.width;
                source: item.imageSource;
            }
        }

        MouseArea {
            id: mouseArea;
            anchors.fill: parent;

            onClicked: selector.currentItem = item
        }
    }
}
