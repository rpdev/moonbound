var Map = {
    Width: 20,
    Height: 20,
    Size: 64
};

var map = new Array( Map.Width * Map.Height );
var decorations = new Array( Map.Width * Map.Height );
var component;

function idx( x, y ) {
    return x * Map.Width + y;
}

function initMap() {
    for ( var i = 0; i < Map.Width * Map.Height; ++i ) {
        if (map[i] != null) {
            map[i].destroy();
        }
        if ( decorations[i] != null ) {
            decorations[i].destroy();
        }
    }

    map = new Array( Map.Width * Map.Height );
    for ( var x = 0; x < Map.Width; ++x ) {
        for ( var y = 0; y < Map.Height; ++y ) {
            map[ idx( x, y ) ] = null;
            createField( x, y );
        }
    }
    decorations = new Array( Map.Width * Map.Height );
}

function createField( x, y ) {
    if ( component == null ) {
        component = Qt.createComponent( "Field.qml" );
    }
    if ( component.status == Component.Ready ) {
        var dynamicObject = component.createObject(background);
        if ( dynamicObject == null ) {
            console.log( "Error creating field!" );
            return false;
        }
        dynamicObject.x = x * Map.Size;
        dynamicObject.y = y * Map.Size;
        dynamicObject.width = Map.Size;
        dynamicObject.height = Map.Size;
        if ( background.fieldType ) {
            dynamicObject.source = background.fieldType;
        }

        map[idx(x,y)] = dynamicObject;
    } else {
        console.log( "Error loading field component" );
        console.log( component.errorString() );
        return false;
    }
    return true;
}

function createDecoration( x, y ) {
    if ( component == null ) {
        component = Qt.createComponent( "Decoration.qml" );
    }
    if ( component.status == Component.Ready ) {
        var dynamicObject = component.createObject(background);
        if ( dynamicObject == null ) {
            console.log( "Error creating decoration!" );
            return false;
        }
        dynamicObject.x = x * Map.Size;
        dynamicObject.y = y * Map.Size;
        dynamicObject.width = Map.Size;
        dynamicObject.height = Map.Size;
        dynamicObject.source = background.decorationType;
        if ( decorations[idx(x,y)] ) {
            decorations[idx(x,y)].destroy();
        }

        decorations[idx(x,y)] = dynamicObject;
    } else {
        console.log( "Error loading field component" );
        console.log( component.errorString() );
        return false;
    }
    return true;
}

function setField( mx, my ) {
    var x = Math.floor(mx / Map.Size);
    var y = Math.floor( my / Map.Size );
    if ( x >= 0 && x < Map.Width && y >= 0 && y < Map.Height ) {
        if ( backgroundButton.checked ) {
            map[ idx( x, y ) ].source = background.fieldType;
        } else if ( decorationButton.checked ) {
            createDecoration( x, y );
        }
    }
}
