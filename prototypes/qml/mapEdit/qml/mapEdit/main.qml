// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "editFuncs.js" as Functions;

Rectangle {
    id: screen
    width: 800
    height: 600

    SystemPalette { id: activePalette }

    Flickable {
        width: parent.width
        anchors { top: parent.top; bottom: toolBar.top }
        contentWidth: background.childrenRect.width
        contentHeight: background.childrenRect.height
        boundsBehavior: Flickable.StopAtBounds

        Item {
            id: background
            width: childrenRect.width
            height: childrenRect.height

            property string fieldType: "../../images/gras.png"
            property string decorationType: "../../images/tree_light_1.png"

            MouseArea {
                anchors.fill: parent

                onClicked: Functions.setField( mouse.x, mouse.y )
            }
        }
    }

    Rectangle {
        id: toolBar
        width: parent.width
        height: childrenRect.height;
        color: activePalette.window
        anchors.bottom: screen.bottom

        Flow {
            id: controls
            width: parent.width
            height: 30
            anchors { top: parent.top; margins: 4 }
            spacing: 4;

            Button {
                text: qsTr( "New Map" )

                onClicked: Functions.initMap();
            }

            RadioButtonGroup {
                id: editMode
            }

            Button {
                id: backgroundButton
                text: qsTr( "Background" )
                checkable: true
                group: editMode
                checked: true
            }

            Button {
                id: decorationButton
                text: qsTr( "Decorations" )
                checkable: true
                group: editMode
            }

            Button {
                text: qsTr( "Quit" )

                onClicked: Qt.quit()
            }
        }

        Item {
            id: imageSelector
            width: parent.width
            height: 70
            anchors.top: controls.bottom
            clip: true

            Flow {
                anchors.fill: parent
                visible: backgroundButton.checked
                ImageSelector {
                    model: [
                        "../../images/gras.png",
                        "../../images/sand.png",
                        "../../images/rock.png",
                        "../../images/swamp.png"
                    ]

                    onCurrentItemChanged: {
                        background.fieldType = currentItem.imageSource;
                    }
                }
            }

            Flow {
                anchors.fill: parent;
                visible: decorationButton.checked;
                ImageSelector {
                    model: [
                        "../../images/tree_light_1.png",
                        "../../images/tree_light_2.png",
                        "../../images/tree_light_3.png",
                        "../../images/tree_light_4.png"
                    ]

                    onCurrentItemChanged: {
                        background.decorationType = currentItem.imageSource;
                    }
                }
            }
        }

    }

}
