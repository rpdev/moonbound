import QtQuick 1.0
import Qt3D 1.0

Viewport {
    width: 1000
    height: 800

    Item3D {
        id: "cube"
        mesh: Mesh {
            source: "meshes/test.3ds"
            options: "ForceSmooth"
        }
        effect: Effect {
            material: china
        }

        Material {
            id: china
            ambientColor: "#c09680"
            specularColor: "#F00000"
            shininess: 128
        }
    }

}
